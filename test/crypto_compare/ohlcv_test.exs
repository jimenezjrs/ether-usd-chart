defmodule CryptoCompare.OHLCVTest do
  use ExUnit.Case

  alias CryptoCompare.OHLCV

  test "builds one empty %Crypto.Compare.OHLCV" do
    response = %OHLCV{}

    assert response.average == nil
    assert response.close == nil
    assert response.high == nil
    assert response.low == nil
    assert response.open == nil
    assert response.time == nil
    assert response.volume_from == nil
    assert response.volume_to ==  nil
  end

  test "builds one %Crypto.Compare.OHLCV struct with data values" do
    response = %CryptoCompare.OHLCV{} = CryptoCompare.OHLCV.build(fixture(:ohlcv))

    average = (response.close + response.high + response.low + response.open) / 4
    assert response.average == average
    assert response.close == 496.74
    assert response.high == 507.49
    assert response.low == 494.01
    assert response.open == 497.22
    assert response.time == 1529193600
    assert response.volume_from == 211122.8
    assert response.volume_to ==  105737440.52
  end

  defp fixture(:ohlcv) do
    %{
      "close" => 496.74,
      "high" => 507.49,
      "low" => 494.01,
      "open" => 497.22,
      "time" => 1529193600,
      "volumefrom" => 211122.8,
      "volumeto" => 105737440.52
    }
  end
end
