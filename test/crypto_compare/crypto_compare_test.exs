defmodule CryptoCompare.CryptoCompareTest do
  use ExUnit.Case

  test "get response from Crypto Compare's API for the last 24 hours" do
    {:ok, response} = CryptoCompare.get("ETH", "USD", :last_24_hours)

    assert response.aggregated == false
    # The API returns the last object with a timestamp for the current day
    # even if data is not complete, that's why we take that into account and 
    # add 1 to the response data objects.
    assert Enum.count(response.data) == 25
    assert response.message == nil
    assert response.status == "Success"
    # We only have three predefined endpoints the depend on the current time.
    # That's why we are just testing that they don't have the default
    # value \\ nil. If we add support for dynamic timestamps then we can test
    # these values directly.
    assert response.time_from != nil
    assert response.time_to != nil
  end

  test "get response from Crypto Compare's API for the last three days" do
    {:ok, response} = CryptoCompare.get("ETH", "USD", :last_three_days)

    assert response.aggregated == false
    # See comment in the test above
    assert Enum.count(response.data) == 4
    assert response.message == nil
    assert response.status == "Success"
    # See comment in the test above
    assert response.time_from != nil
    assert response.time_to != nil
  end

  test "get response from Crypto Compare's API for the last seven days" do
    {:ok, response} = CryptoCompare.get("ETH", "USD", :last_seven_days)

    assert response.aggregated == false
    # See comment in the test above
    assert Enum.count(response.data) == 8
    assert response.message == nil
    assert response.status == "Success"
    # See comment in the test above
    assert response.time_from != nil
    assert response.time_to != nil
  end
end
