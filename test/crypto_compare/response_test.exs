defmodule CryptoCompare.ResponseTest do
  use ExUnit.Case

  test "builds empty %CryptoCompare.Response struct" do
    response = %CryptoCompare.Response{}

    assert response.aggregated == false
    assert Enum.count(response.data) == 0
    assert response.message == nil
    assert response.status == nil
    assert response.time_from == nil
    assert response.time_to == nil
  end

  test "builds one %CryptoCompare.Response with :ok atom when the api returns success" do
    {:ok, response } =
      crypto_compare_success_fixture()
      |> CryptoCompare.Response.build()

    assert response.aggregated == false
    assert Enum.count(response.data) == 2
    assert response.message == nil
    assert response.status == "Success"
    assert response.time_from == 1529193600
    assert response.time_to == 1529280000
  end

  test "builds one %Crypto.Compare.Response with :error atom when the api returns an error" do
    {:error, response} =
      crypto_compare_failure_fixture()
      |> CryptoCompare.Response.build()

    assert response.aggregated == false
    assert Enum.count(response.data) == 0
    assert response.message != nil
    assert response.status == "Error"
    assert response.time_from == nil
    assert response.time_to == nil
  end

  defp crypto_compare_success_fixture do
    %{
      "Aggregated" => false,
      "ConversionType" => %{
        "conversionSymbol" => "",
        "type" => "direct"
      },
      "Data" => [
        %{
          "close" => 496.74,
          "high" => 507.49,
          "low" => 494.01,
          "open" => 497.22,
          "time" => 1529193600,
          "volumefrom" => 211122.8,
          "volumeto" => 105737440.52
        },
        %{
          "close" => 493.88,
          "high" => 497.03,
          "low" => 487.72,
          "open" => 496.84,
          "time" => 1529280000,
          "volumefrom" => 36394.68,
          "volumeto" => 17898907.52
        }
      ],
      "FirstValueInArray" => true,
      "Response" => "Success",
      "TimeFrom" => 1529193600,
      "TimeTo" => 1529280000,
      "Type" => 100
    }
  end

  defp crypto_compare_failure_fixture do
    %{
      "Aggregated" => false,
      "Data" => [],
      "Message" => "toTs param is not an integer, not a valid timestamp.",
      "Response" => "Error",
      "Type" => 1
    }
  end
end
