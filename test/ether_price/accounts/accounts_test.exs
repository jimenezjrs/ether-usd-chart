defmodule EtherPrice.AccountsTest do
  use EtherPrice.DataCase

  alias EtherPrice.Accounts

  describe "users" do
    alias EtherPrice.Accounts.User

    @valid_attrs %{email: "some email", password: "some password"}
    # @update_attrs %{email: "some updated email", password: "some updated password"}
    @invalid_attrs %{email: nil, password: nil}

    def user_fixture(attrs \\ %{}) do
      {:ok, user} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Accounts.create_user()

      user
    end

    test "get_user!/1 returns the user with given id" do
      user = user_fixture()
      assert Accounts.get_user!(user.id).email == user.email
      assert Accounts.get_user!(user.id).password_hash == user.password_hash
    end

    test "list_users/0 returns all users" do
      user = user_fixture()
      user = Accounts.get_user!(user.id) # create user returns virtual password
      assert Accounts.list_users() == [user]
    end

    test "create_user/1 with valid data creates a user" do
      assert {:ok, %User{} = user} = Accounts.create_user(@valid_attrs)
      assert user.email == "some email"
      assert user.password == "some password"
    end

    test "create_user/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Accounts.create_user(@invalid_attrs)
    end
  end
end
