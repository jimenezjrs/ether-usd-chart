defmodule EtherPriceWeb.PageControllerTest do
  use EtherPriceWeb.ConnCase

  test "GET /", %{conn: conn} do
    conn = get conn, "/"
    assert html_response(conn, 200) =~ "EtherPrice"
  end
end
