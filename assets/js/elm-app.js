import Elm from "./ether_price"

const appContainer = document.getElementById("elm__app")
let app = null

if (appContainer) {
  app = Elm.Main.embed(appContainer)
}

export default app
