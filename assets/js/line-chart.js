const renderChart = (id, data) => {
  _removeChart(id)
  _addChart(data)
}

const _removeChart = (id) => {
  const elem = document.getElementById(id)
  if(elem) {
    while (elem.firstChild) {
      elem.removeChild(elem.firstChild);
    }
  }
}

const _addChart = (data) => {
  const chartElem = document.getElementById("chart")

  if (chartElem) {
    const newChartElement = document.createElement("canvas")
    newChartElement.setAttribute("id", "chart__element")
    newChartElement.setAttribute("style", "width: 400px; height: 200px;")

    chartElem.appendChild(newChartElement)

    const formattedData = formatData(data)
    _initializeChart(newChartElement.getContext('2d'), formattedData)
  }
}


const _initializeChart = (chartElem, chartData) => {
  _renderChart(chartElem, chartData)
  return []
}

const _renderChart = (chartElem, chartData) => {
  return new Chart(chartElem, {
    type: 'line',
    data: chartData,
    options: opts
  })
}

const opts = {
  legend: {
    labels: {
      fontColor: "rgba(51, 51, 51, 1)",
      fontSize: 16
    }
  },
  scales: {
    xAxes: [{
      ticks: {
        fontColor: "rgba(51, 51, 51, 1)",
        fontSize: 16
      }
    }],
    yAxes: [{
      ticks: {
        beginAtZero: true,
        fontColor: "rgba(51, 51, 51, 1)",
        fontSize: 16
      }
    }]
  }
}

const formatData = (data) => {
  const labels = []
  const datasets = [{
    label: 'ETH / USD',
    data: [],
    backgroundColor: [
      // 'rgba(245, 0, 87, 0.5)',
      'rgba(83, 59, 235, 0.7)',
    ],
    borderColor: [
      // 'rgba(245, 0, 87, 1)',
      'rgba(83, 59, 235, 1)',
    ],
    borderWidth: 1
  }]

  for(let i=0; i < data.length; i++) {
    const point = {x: i, y: data[i].average}
    labels.push(formatDate(data[i].time))
    datasets[0].data.push(point)
  }

  return {
    labels,
    datasets,
  }
}

const formatDate = (timestamp) => {
  const time = parseInt(timestamp) * 1000
  const date = new Date(time)

  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minutes = date.getMinutes() + 1

  return `${day}/${month}/${day} ${hour}:00`
}

export { renderChart }

/*
var ctx = document.getElementById("EtherPrice__chart").getContext('2d');
var myChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: [
          'January',
          'February',
          'March',
          'April',
          'May',
          'June',
          'July',
          'August',
          'September',
          'October',
          'November',
          'December'
        ],
        datasets: [{
            label: 'ETH / USD',
            data: [
              {x: 1, y: 12},
              {x: 2, y: 19},
              {x: 3, y: 3},
              {x: 4, y: 3},
              {x: 5, y: 3},
              {x: 6, y: 3},
              {x: 7, y: 3},
              {x: 8, y: 3},
              {x: 9, y: 3},
              {x: 10, y: 3},
              {x: 11, y: 3},
              {x: 12, y: 83},
            ],
            backgroundColor: [
              'rgba(245, 0, 87, 0.5)',
            ],
            borderColor: [
              'rgba(245, 0, 87, 1)',
            ],
            borderWidth: 1
        }]
    },
    options: {
      legend: {
        labels: {
          fontColor: "white",
          fontSize: 14
        }
      },
      scales: {
        xAxes: [{
          ticks: {
            fontColor: "white",
            fontSize: 14
          }
        }],
        yAxes: [{
          ticks: {
            beginAtZero: true,
            fontColor: "white",
            fontSize: 14
          }
        }]
      }
    }
});
*/
