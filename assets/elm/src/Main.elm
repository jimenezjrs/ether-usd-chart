module Main exposing (..)

-- import Html.Attributes exposing (..)
-- import Html.Events exposing (onClick)
-- import Http
-- import ViewHelpers exposing (..)
-- UPDATE
-- import Models exposing (Model, initialModel, CryptoResponse)

import Models exposing (Model, initialModel, Msg)


-- import Html exposing (program)

import Update exposing (update)
import Cmds exposing (getLast24HoursData)
import View exposing (view)
import Navigation exposing (Location)
import Routing


-- INIT


init : Location -> ( Model, Cmd Msg )
init location =
    let
        currentRoute =
            Routing.parseLocation location
    in
        ( initialModel currentRoute, getLast24HoursData )


main : Program Never Model Msg
main =
    Navigation.program Models.OnLocationChange
        { init = init
        , view = view
        , update = update
        , subscriptions = (\_ -> Sub.none)
        }
