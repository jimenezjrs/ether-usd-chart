module Auth.Signup exposing (..)

import Models exposing (..)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick, onInput)
import ViewHelpers exposing (navbar, viewAlertMessage, viewErrorMessage)


form : Model -> Html Msg
form model =
    let
        isSignUpFormValid =
            model.signUpForm.isEmailValid
                && model.signUpForm.isPasswordValid
                && model.signUpForm.isPasswordConfirmationValid

        disableFormBtn =
            not isSignUpFormValid
    in
        div [ class "form__container" ]
            [ div [ class "form" ]
                [ div [ class "form__content" ]
                    [ h1 [] [ text "Sign Up" ]
                    , viewAlertMessage model.alertMessage
                    , input
                        [ type_ "email"
                        , placeholder "Email"
                        , value model.signUpForm.email
                        , onInput CheckSignUpEmailErrors
                        , autofocus True
                        ]
                        []
                    , viewErrorMessage model.signUpForm.emailError
                    , input
                        [ type_ "password"
                        , placeholder "Password"
                        , value model.signUpForm.password
                        , onInput CheckSignUpPasswordErrors
                        ]
                        []
                    , viewErrorMessage model.signUpForm.passwordError
                    , input
                        [ type_ "password"
                        , placeholder "Confirm your password"
                        , value model.signUpForm.passwordConfirmation
                        , onInput CheckSignUpPasswordConfirmationErrors
                        ]
                        []
                    , viewErrorMessage model.signUpForm.passwordConfirmationError
                    ]
                , button
                    [ onClick SubmitSignUpForm
                    , disabled disableFormBtn
                    ]
                    [ text "Continue" ]
                ]
            ]


view : Model -> Html Msg
view model =
    div []
        [ navbar model.user
        , main_ [ class "container", attribute "role" "main" ]
            [ form model ]
        ]
