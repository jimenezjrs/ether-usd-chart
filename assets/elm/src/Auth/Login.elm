module Auth.Login exposing (..)

import Models exposing (..)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick, onInput)
import ViewHelpers exposing (navbar, viewAlertMessage)


form : Model -> Html Msg
form model =
    let
        emailLength =
            String.length model.loginForm.email

        passwordLength =
            String.length model.loginForm.password

        isLoginFormValid =
            (emailLength > 0 && passwordLength > 0)

        disableFormBtn =
            not isLoginFormValid
    in
        div [ class "form__container" ]
            [ div [ class "form" ]
                [ div [ class "form__content" ]
                    [ h1 [] [ text "Login" ]
                    , viewAlertMessage model.alertMessage
                    , input
                        [ type_ "text"
                        , placeholder "Email"
                        , value model.loginForm.email
                        , onInput TypingEmail
                        , autofocus True
                        ]
                        []
                    , input
                        [ type_ "password"
                        , placeholder "Password"
                        , value model.loginForm.password
                        , onInput TypingPassword
                        ]
                        []
                    ]
                , button
                    [ onClick SubmitLoginForm
                    , disabled disableFormBtn
                    ]
                    [ text "Continue" ]
                ]
            ]


view : Model -> Html Msg
view model =
    div []
        [ navbar model.user
        , main_ [ class "container", attribute "role" "main" ]
            [ form model ]
        ]
