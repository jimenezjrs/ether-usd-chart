module Update exposing (..)

import Models exposing (..)
import Cmds exposing (..)
import Ports exposing (renderChart)
import Routing exposing (..)
import Http
import Navigation


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        OnLocationChange location ->
            let
                newRoute =
                    parseLocation location

                previousUser =
                    model.user

                newModel =
                    initialModel newRoute
            in
                case newRoute of
                    Models.Chart ->
                        if model.user == Nothing then
                            ( model, Navigation.modifyUrl loginPath )
                        else
                            ( { newModel | user = previousUser }, getLast24HoursData )

                    Models.Logout ->
                        ( newModel, Navigation.modifyUrl loginPath )

                    _ ->
                        ( { newModel | user = previousUser }, Cmd.none )

        TwentyFourHoursChart ->
            ( { model
                | chartData = initialCryptoResponse
                , chartName = TwentyFourHoursChart
                , chartTitle = "Last 24 Hours"
              }
            , getLast24HoursData
            )

        ThreeDaysChart ->
            ( { model
                | chartData = initialCryptoResponse
                , chartName = ThreeDaysChart
                , chartTitle = "Last 3 Days"
              }
            , getLastThreeDaysData
            )

        SevenDaysChart ->
            ( { model
                | chartData = initialCryptoResponse
                , chartName = SevenDaysChart
                , chartTitle = "Last 7 Days"
              }
            , getLastSevenDaysData
            )

        RenderChart ->
            ( model, (renderChart model.chartData) )

        SubmitLoginForm ->
            ( model, postLoginForm model.loginForm )

        LoginFormResponse (Ok user) ->
            ( { model | user = Just user }, Navigation.modifyUrl chartPath )

        LoginFormResponse (Err error) ->
            let
                errorMessage =
                    "Email and/or password invalid."
            in
                ( { model | alertMessage = Just errorMessage }, Cmd.none )

        TypingEmail givenEmail ->
            let
                oldLoginForm =
                    model.loginForm

                newLoginForm =
                    { oldLoginForm | email = givenEmail }
            in
                ( { model | loginForm = newLoginForm }, Cmd.none )

        TypingPassword givenPassword ->
            let
                oldLoginForm =
                    model.loginForm

                newLoginForm =
                    { oldLoginForm | password = givenPassword }
            in
                ( { model | loginForm = newLoginForm }, Cmd.none )

        SubmitSignUpForm ->
            ( model, postSignUpForm model.signUpForm )

        SignUpFormResponse (Ok user) ->
            ( { model | user = Just user }, Navigation.modifyUrl chartPath )

        SignUpFormResponse (Err error) ->
            let
                errorMessage =
                    handleApiErrors error
            in
                ( { model | alertMessage = Just errorMessage }, Cmd.none )

        CheckSignUpEmailErrors givenEmail ->
            let
                oldSignUpForm =
                    model.signUpForm

                tmpSignUpForm =
                    { oldSignUpForm | email = givenEmail }

                ( isEmailValid_, errorMessage ) =
                    checkEmailErrors givenEmail

                newSignUpForm =
                    { tmpSignUpForm
                        | isEmailValid = isEmailValid_
                        , emailError = errorMessage
                    }
            in
                ( { model | signUpForm = newSignUpForm }, Cmd.none )

        CheckSignUpPasswordErrors givenPassword ->
            let
                oldSignUpForm =
                    model.signUpForm

                tmpSignUpForm =
                    { oldSignUpForm | password = givenPassword }

                ( isPasswordValid_, errorMessage ) =
                    checkPasswordErrors tmpSignUpForm

                newSignUpForm =
                    { tmpSignUpForm
                        | isPasswordValid = isPasswordValid_
                        , passwordError = errorMessage
                    }
            in
                ( { model | signUpForm = newSignUpForm }, Cmd.none )

        CheckSignUpPasswordConfirmationErrors givenPasswordConfirmation ->
            let
                oldSignUpForm =
                    model.signUpForm

                tmpSignUpForm =
                    { oldSignUpForm | passwordConfirmation = givenPasswordConfirmation }

                ( isPasswordConfirmationValid_, errorMessage ) =
                    checkPasswordConfirmationErrors tmpSignUpForm

                newSignUpForm =
                    { tmpSignUpForm
                        | isPasswordConfirmationValid = isPasswordConfirmationValid_
                        , passwordConfirmationError = errorMessage
                    }
            in
                ( { model | signUpForm = newSignUpForm }, Cmd.none )

        NewCryptoResponse (Ok cryptoResponseData) ->
            ( { model | chartData = cryptoResponseData }, renderChart cryptoResponseData )

        NewCryptoResponse (Err error) ->
            let
                errorMessage =
                    handleApiErrors error
            in
                ( { model | alertMessage = Just errorMessage }, Cmd.none )

        CloseAlert ->
            ( { model | alertMessage = Nothing }, Cmd.none )


checkEmailErrors : String -> ( Bool, Maybe String )
checkEmailErrors email =
    let
        response =
            if not (String.contains "@" email) then
                ( False, Just "The email format is invalid." )
            else
                ( True, Nothing )
    in
        response


checkPasswordErrors : SignUpForm -> ( Bool, Maybe String )
checkPasswordErrors form =
    let
        response =
            if String.length form.password < 6 then
                ( False, Just "The password is too short." )
            else
                ( True, Nothing )
    in
        response


checkPasswordConfirmationErrors : SignUpForm -> ( Bool, Maybe String )
checkPasswordConfirmationErrors form =
    let
        response =
            if form.password /= form.passwordConfirmation then
                ( False, Just "Passwords didn't match." )
            else
                ( True, Nothing )
    in
        response


handleApiErrors : Http.Error -> String
handleApiErrors error =
    case error of
        Http.NetworkError ->
            "I can't connect to the server"

        Http.Timeout ->
            "Request timed out"

        Http.BadStatus response ->
            case response.status.code of
                400 ->
                    "Make sure your credentials are valid."

                _ ->
                    "Unexpected Error"

        Http.BadPayload message _ ->
            "Something went wrong: " ++ message

        _ ->
            (toString error)
