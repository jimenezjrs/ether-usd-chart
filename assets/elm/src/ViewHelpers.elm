module ViewHelpers exposing (..)

import Models exposing (..)
import Html exposing (..)
import Html.Attributes exposing (..)
import Date
import Routing exposing (loginPath, signupPath, chartPath, logoutPath)


navbar : Maybe User -> Html Msg
navbar user =
    header []
        [ nav []
            [ span [ class "brand" ]
                [ text "Amazix" ]
            , viewLinks user
            ]
        ]


viewLinks : Maybe User -> Html Msg
viewLinks user =
    case user of
        Nothing ->
            ul []
                [ li []
                    [ a [ href loginPath ]
                        [ text "Log in" ]
                    ]
                , li []
                    [ a [ href signupPath ]
                        [ text "Sign up" ]
                    ]
                ]

        Just value ->
            ul []
                [ li []
                    [ a [ href logoutPath ]
                        [ text "Log out" ]
                    ]
                ]


viewErrorMessage : Maybe String -> Html msg
viewErrorMessage errorMessage =
    case errorMessage of
        Nothing ->
            text ""

        Just value ->
            fieldError value


fieldError : String -> Html msg
fieldError errorMessage =
    p [ class "field_error" ]
        [ text errorMessage ]


viewAlertMessage : Maybe String -> Html Msg
viewAlertMessage alertMessage =
    case alertMessage of
        Just message ->
            div [ class "alert" ]
                [ text message ]

        Nothing ->
            text ""


tableHead : String -> Html msg
tableHead value =
    th []
        [ text value ]


tableHeader : Html msg
tableHeader =
    thead []
        [ tr []
            [ tableHead "Open"
            , tableHead "High"
            , tableHead "Low"
            , tableHead "Close"
            , tableHead "Average"
            , tableHead "Time"
            ]
        ]


tableData : String -> Html msg
tableData value =
    td []
        [ text value ]


timeStampToDate : String -> String
timeStampToDate timeStamp =
    case String.toFloat timeStamp of
        Ok parsedTimeStamp ->
            (parsedTimeStamp * 1000)
                |> Date.fromTime
                |> toString

        Err error ->
            "N/A"
