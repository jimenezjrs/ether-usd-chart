module Chart.Chart exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick)
import Models exposing (Model, Msg(..), CryptoResponse, OHLCV)
import ViewHelpers exposing (..)


-- VIEW


tableRow : OHLCV -> Html msg
tableRow timeFrameData =
    tr []
        [ tableData (toString timeFrameData.open)
        , tableData (toString timeFrameData.high)
        , tableData (toString timeFrameData.low)
        , tableData (toString timeFrameData.close)
        , tableData (toString timeFrameData.average)
        , tableData (timeStampToDate timeFrameData.time)
        ]


tableBody : List OHLCV -> Html msg
tableBody data =
    let
        listOfOHLCVItems =
            (List.map tableRow data)
    in
        tbody [] listOfOHLCVItems


table_ : List OHLCV -> Html msg
table_ data =
    table []
        [ tableHeader
        , tableBody data
        ]


viewTable : Model -> Html msg
viewTable model =
    div [ id "EtherPrice__content" ]
        [ section [ class "card" ]
            [ h1 []
                [ text model.chartTitle ]
            , table_ model.chartData.data
            ]
        ]


viewChart : Model -> Html Msg
viewChart model =
    div [ id "ether_price_app" ]
        [ div [ id "chart__container" ]
            [ navbar model.user
            , div [ classList [ ( "error__container", model.alertMessage /= Nothing ) ] ]
                [ viewAlertMessage model.alertMessage ]
            , div [ class "chart__type_group" ]
                [ button
                    [ (onClick TwentyFourHoursChart)
                    , classList
                        [ ( "left", True )
                        , ( "active", model.chartName == TwentyFourHoursChart )
                        ]
                    ]
                    [ text "24 hours" ]
                , button
                    [ (onClick ThreeDaysChart)
                    , classList
                        [ ( "center", True )
                        , ( "active", model.chartName == ThreeDaysChart )
                        ]
                    ]
                    [ text "3 days" ]
                , button
                    [ (onClick SevenDaysChart)
                    , classList
                        [ ( "right", True )
                        , ( "active", model.chartName == SevenDaysChart )
                        ]
                    ]
                    [ text "7 days" ]
                ]
            , div [ id "chart" ]
                []
            ]
        ]


view : Model -> Html Msg
view model =
    main_ [ attribute "role" "main" ]
        [ viewChart model
        , viewTable model
        ]
