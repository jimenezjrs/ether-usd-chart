module Models exposing (..)

import Http
import Navigation exposing (Location)


type Msg
    = OnLocationChange Location
    | TwentyFourHoursChart
    | ThreeDaysChart
    | SevenDaysChart
    | RenderChart
    | NewCryptoResponse (Result Http.Error CryptoResponse)
    | CloseAlert
    | SubmitLoginForm
    | SubmitSignUpForm
    | LoginFormResponse (Result Http.Error User)
    | SignUpFormResponse (Result Http.Error User)
    | TypingEmail String
    | TypingPassword String
    | CheckSignUpEmailErrors String
    | CheckSignUpPasswordErrors String
    | CheckSignUpPasswordConfirmationErrors String


type Route
    = Login
    | Logout
    | Signup
    | Chart
    | NotFoundRoute


type alias Model =
    { chartName : Msg
    , chartTitle : String
    , chartData : CryptoResponse
    , loginForm : LoginForm
    , signUpForm : SignUpForm
    , alertMessage : Maybe String
    , user : Maybe User
    , route : Route
    }


initialModel : Route -> Model
initialModel route =
    { chartName = TwentyFourHoursChart
    , chartTitle = "Last 24 Hours"
    , chartData = initialCryptoResponse
    , loginForm = initialLoginForm
    , signUpForm = initialSignUpForm
    , alertMessage = Nothing
    , user = Nothing
    , route = route
    }


type alias User =
    { email : String
    , token : String
    }


type alias LoginForm =
    { email : String
    , password : String
    }


initialLoginForm : LoginForm
initialLoginForm =
    { email = ""
    , password = ""
    }


type alias SignUpForm =
    { email : String
    , password : String
    , passwordConfirmation : String
    , isEmailValid : Bool
    , emailError : Maybe String
    , isPasswordValid : Bool
    , passwordError : Maybe String
    , isPasswordConfirmationValid : Bool
    , passwordConfirmationError : Maybe String
    }


initialSignUpForm : SignUpForm
initialSignUpForm =
    { email = ""
    , password = ""
    , passwordConfirmation = ""
    , isEmailValid = False
    , emailError = Nothing
    , isPasswordValid = False
    , passwordError = Nothing
    , isPasswordConfirmationValid = False
    , passwordConfirmationError = Nothing
    }


type alias CryptoResponse =
    { aggregated : Bool
    , data : List OHLCV
    , message : String
    , status : String
    , timeFrom : String
    , timeTo : String
    }


initialCryptoResponse : CryptoResponse
initialCryptoResponse =
    { aggregated = False
    , data = []
    , message = ""
    , status = ""
    , timeFrom = ""
    , timeTo = ""
    }


type alias OHLCV =
    { average : Float
    , close : Float
    , high : Float
    , low : Float
    , open : Float
    , time : String
    , volumeFrom : Float
    , volumeTo : Float
    }


initialOHLCV : OHLCV
initialOHLCV =
    { average = 0.0
    , close = 0.0
    , high = 0.0
    , low = 0.0
    , open = 0.0
    , time = ""
    , volumeFrom = 0.0
    , volumeTo = 0.0
    }


sampleData : List OHLCV
sampleData =
    [ OHLCV 537.4250000000001 538.62 538.99 536.01 536.08 "1529542800" 8159.97 4383550.49
    , OHLCV 537.4250000000001 538.62 538.99 536.01 536.08 "1529542800" 8159.97 4383550.49
    ]
