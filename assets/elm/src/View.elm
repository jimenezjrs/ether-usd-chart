module View exposing (..)

import Models exposing (Model, Msg)
import Auth.Signup
import Auth.Login
import Chart.Chart
import Html exposing (Html, div, h1, text)
import Html.Attributes exposing (class)


view : Model -> Html Msg
view model =
    div []
        [ page model ]


page : Model -> Html Msg
page model =
    case model.route of
        Models.Login ->
            Auth.Login.view model

        Models.Signup ->
            Auth.Signup.view model

        Models.Chart ->
            Chart.Chart.view model

        Models.Logout ->
            Auth.Login.view model

        Models.NotFoundRoute ->
            notFoundView


notFoundView : Html msg
notFoundView =
    div [] [ h1 [ class "not_found" ] [ text "Not Found" ] ]
