module Encoders exposing (..)

import Models exposing (LoginForm, SignUpForm)
import Json.Encode as Encode


loginFormEncoder : LoginForm -> Encode.Value
loginFormEncoder form =
    Encode.object
        [ ( "email", Encode.string form.email )
        , ( "password", Encode.string form.password )
        ]


signUpFormEncoder : SignUpForm -> Encode.Value
signUpFormEncoder form =
    Encode.object
        [ ( "email", Encode.string form.email )
        , ( "password", Encode.string form.password )
        ]
