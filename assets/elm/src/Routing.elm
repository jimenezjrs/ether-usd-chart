module Routing exposing (..)

import Navigation exposing (Location)
import Models exposing (Route(..))
import UrlParser exposing (..)


loginPath : String
loginPath =
    "#"


logoutPath : String
logoutPath =
    "#logout"


signupPath : String
signupPath =
    "#signup"


chartPath : String
chartPath =
    "#chart"


matchers : Parser (Route -> a) a
matchers =
    oneOf
        [ map Login top
        , map Logout (s "logout")
        , map Signup (s "signup")
        , map Chart (s "chart")
        ]


parseLocation : Location -> Route
parseLocation location =
    case (parseHash matchers location) of
        Just route ->
            route

        Nothing ->
            NotFoundRoute
