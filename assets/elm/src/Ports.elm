port module Ports exposing (..)

import Models exposing (CryptoResponse)


port renderChart : CryptoResponse -> Cmd msg
