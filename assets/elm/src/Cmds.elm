module Cmds exposing (..)

import Models exposing (Model, LoginForm, SignUpForm, Msg(..))
import Http
import Decoders exposing (..)
import Encoders exposing (loginFormEncoder, signUpFormEncoder)


apiUrlPrefix : String
apiUrlPrefix =
    "http://localhost:4000"


last24HoursUrl : String
last24HoursUrl =
    apiUrlPrefix ++ "/api/price/last24hours"


getLast24HoursData : Cmd Msg
getLast24HoursData =
    cryptoDecoder
        |> Http.get last24HoursUrl
        |> Http.send NewCryptoResponse


lastThreeDaysUrl : String
lastThreeDaysUrl =
    apiUrlPrefix ++ "/api/price/last3days"


getLastThreeDaysData : Cmd Msg
getLastThreeDaysData =
    cryptoDecoder
        |> Http.get lastThreeDaysUrl
        |> Http.send NewCryptoResponse


lastSevenDaysUrl : String
lastSevenDaysUrl =
    apiUrlPrefix ++ "/api/price/last7days"


getLastSevenDaysData : Cmd Msg
getLastSevenDaysData =
    cryptoDecoder
        |> Http.get lastSevenDaysUrl
        |> Http.send NewCryptoResponse


postLoginForm : LoginForm -> Cmd Msg
postLoginForm form =
    let
        url =
            apiUrlPrefix ++ "/api/users/login"

        body =
            loginFormEncoder form
                |> Http.jsonBody

        request =
            Http.post url body loginFormDecoder
    in
        Http.send LoginFormResponse request


postSignUpForm : SignUpForm -> Cmd Msg
postSignUpForm form =
    let
        url =
            apiUrlPrefix ++ "/api/users/signup"

        body =
            signUpFormEncoder form
                |> Http.jsonBody

        request =
            Http.post url body signUpFormDecoder
    in
        Http.send SignUpFormResponse request
