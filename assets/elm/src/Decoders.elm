module Decoders exposing (..)

import Models exposing (User, CryptoResponse, OHLCV)
import Json.Decode as Decode exposing (Decoder, field)


loginFormDecoder : Decoder User
loginFormDecoder =
    Decode.map2 User
        (field "email" Decode.string)
        (field "token" Decode.string)


signUpFormDecoder : Decoder User
signUpFormDecoder =
    Decode.map2 User
        (field "email" Decode.string)
        (field "token" Decode.string)


ohlcvDecoder : Decoder OHLCV
ohlcvDecoder =
    Decode.map8 OHLCV
        (field "average" Decode.float)
        (field "close" Decode.float)
        (field "high" Decode.float)
        (field "low" Decode.float)
        (field "open" Decode.float)
        (field "time" Decode.string)
        (field "volume_from" Decode.float)
        (field "volume_to" Decode.float)


cryptoDecoder : Decoder CryptoResponse
cryptoDecoder =
    Decode.map6 CryptoResponse
        (field "aggregated" Decode.bool)
        (field "data" (Decode.list ohlcvDecoder))
        (field "message" Decode.string)
        (field "status" Decode.string)
        (field "time_from" Decode.string)
        (field "time_to" Decode.string)
