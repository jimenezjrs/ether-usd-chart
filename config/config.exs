# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :ether_price,
  ecto_repos: [EtherPrice.Repo]

# Configures the endpoint
config :ether_price, EtherPriceWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "Y4fNLtYoLwgg3eOsaEdlfFLqFXRKhijdAvm2p3b2n5JYcOIDqnp921m+ug1y1FrS",
  render_errors: [view: EtherPriceWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: EtherPrice.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
