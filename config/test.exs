use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :ether_price, EtherPriceWeb.Endpoint,
  http: [port: 4001],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Reduce the number of rounds to keep tests fast
config :bcrypt_elixir, log_rounds: 4

# Configure your database
config :ether_price, EtherPrice.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "ether_price_user",
  password: "nexus-upstroke-taboo",
  database: "ether_price_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox
