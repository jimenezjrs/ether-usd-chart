defmodule CryptoCompare.Response do
  alias CryptoCompare.Response
  alias CryptoCompare.OHLCV

  defstruct aggregated: false, data: [], message: "", status: "",
    time_from: nil, time_to: nil

  @params [:aggregated, :data, :message, :status, :time_from, :time_to]
  def build(data) do
    case status = Map.get(data, "Response") do
      "Success" ->
        {:ok, Enum.reduce(@params, %Response{}, &get_attribute(&2, data, &1))}
      _ ->
        response =
          %Response{}
          |> get_attribute(data, :message)
          |> get_attribute(data, :status)

        {:error, response}
    end
  end

  defp get_attribute(response = %Response{}, data, key = :data) do
    value =
      data
      |> Map.get("Data")
      |> Enum.map(&OHLCV.build(&1))

    Map.put(response, key, value)
  end

  defp get_attribute(response = %Response{}, data, key = :aggregated) do
    value = Map.get(data, "Aggregated")
    Map.put(response, key, value)
  end

  defp get_attribute(response = %Response{}, data, key = :status) do
    value = Map.get(data, "Response")
    Map.put(response, key, value)
  end

  defp get_attribute(response = %Response{}, data, key = :time_from) do
    value = Map.get(data, "TimeFrom")
    Map.put(response, key, value)
  end

  defp get_attribute(response = %Response{}, data, key = :time_to) do
    value = Map.get(data, "TimeTo")
    Map.put(response, key, value)
  end

  defp get_attribute(response = %Response{}, data, key = :message) do
    value = Map.get(data, "Message", "")
    Map.put(response, key, value)
  end
end
