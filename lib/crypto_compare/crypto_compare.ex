defmodule CryptoCompare do
  require Logger

  alias CryptoCompare.Response

  @daily_endpoint "https://min-api.cryptocompare.com/data/histoday?"
  @hourly_endpoint "https://min-api.cryptocompare.com/data/histohour?"

  def get(currency_from, currency_to, :last_24_hours) do
    request = build_request(currency_from, currency_to, 24, :hourly)
    parse_response(request)
  end

  def get(currency_from, currency_to, :last_three_days) do
    request = build_request(currency_from, currency_to, 3, :daily)
    parse_response(request)
  end

  def get(currency_from, currency_to, :last_seven_days) do
    request = build_request(currency_from, currency_to, 7, :daily)
    parse_response(request)
  end

  defp parse_response(request) do
    Logger.debug("[Crypto Compare Request]: #{request}")

    case HTTPoison.get(request) do
      {:ok, %HTTPoison.Response{body: data, status_code: 200}} ->
        data
        |> Poison.decode!()
        |> CryptoCompare.Response.build()
      _ ->
        %CryptoCompare.Response{}
    end
  end

  defp build_request(currency_from, currency_to, limit, :hourly) do
    @hourly_endpoint
    <> "fsym=#{currency_from}&"
    <> "tsym=#{currency_to}&"
    <> "limit=#{limit}"
  end

  defp build_request(currency_from, currency_to, limit, :daily) do
    @daily_endpoint
    <> "fsym=#{currency_from}&"
    <> "tsym=#{currency_to}&"
    <> "limit=#{limit}"
  end
end
