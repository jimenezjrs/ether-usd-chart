defmodule CryptoCompare.OHLCV do
  alias CryptoCompare.OHLCV

  defstruct average: nil, close: nil, high: nil, low: nil, open: nil, time: nil,
    volume_from: nil, volume_to: nil

  @params [:close, :high, :low, :open, :time, :volume_from, :volume_to, :average]

  def build(data) do
    Enum.reduce(@params, %OHLCV{}, &get_attribute(&2, data, &1))
  end

  defp get_attribute(%{open: open, high: high, low: low, close: close} = response, data, key = :average) do
    # compute average as ohlc average, this can be optimized to support other
    # types of average calculation via an extra param.
    average = (open + high + low + close) / 4
    Map.put(response, key, average)
  end

  defp get_attribute(response, data, key = :close) do
    value = Map.get(data, "close")
    Map.put(response, key, value)
  end

  defp get_attribute(response, data, key = :high) do
    value = Map.get(data, "high")
    Map.put(response, key, value)
  end

  defp get_attribute(response, data, key = :low) do
    value = Map.get(data, "low")
    Map.put(response, key, value)
  end

  defp get_attribute(response, data, key = :open) do
    value = Map.get(data, "open")
    Map.put(response, key, value)
  end

  defp get_attribute(response, data, key = :time) do
    value = Map.get(data, "time")
    Map.put(response, key, value)
  end

  defp get_attribute(response, data, key = :volume_from) do
    value = Map.get(data, "volumefrom")
    Map.put(response, key, value)
  end

  defp get_attribute(response, data, key = :volume_to) do
    value = Map.get(data, "volumeto")
    Map.put(response, key, value)
  end
end
