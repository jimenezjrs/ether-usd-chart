defmodule EtherPriceWeb.SessionView do
  use EtherPriceWeb, :view

  def render("login.json", %{user: user, token: token}) do
    user_json(user, token)
  end

  def render("errors.json", _) do
    errors_json()
  end

  defp errors_json() do
    %{
      errors: "Email and/or password are invalid."
    }
  end

  defp user_json(user, token) do
    %{
      email: user.email,
      token: token
    }
  end
end
