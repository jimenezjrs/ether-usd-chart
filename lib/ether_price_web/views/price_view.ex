defmodule EtherPriceWeb.PriceView do
  use EtherPriceWeb, :view

  def render("last_24_hours.json", %{data: data}) do
    data_json(data)
  end

  def render("last_three_days.json", %{data: data}) do
    data_json(data)
  end

  def render("last_seven_days.json", %{data: data}) do
    data_json(data)
  end

  defp data_json(data) do
    %{
      aggregated: data.aggregated,
      data: Enum.map(data.data, &price_json/1),
      message: data.message,
      status: data.status,
      time_from: "#{data.time_from}",
      time_to: "#{data.time_to}",
    }
  end

  defp price_json(price) do
    %{
      average: price.average,
      close: price.close,
      high: price.high,
      low: price.low,
      open: price.open,
      time: "#{price.time}",
      volume_from: price.volume_from,
      volume_to: price.volume_to,
    }
  end
end
