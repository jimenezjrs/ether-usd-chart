defmodule EtherPriceWeb.UserView do
  use EtherPriceWeb, :view

  def render("signup.json", %{user: user, token: token}) do
    user_json(user, token)
  end

  def render("errors.json", %{changeset: changeset}) do
    errors_json(changeset)
  end

  defp user_json(user, token) do
    %{
      email: user.email,
      token: token
    }
  end

  defp errors_json(changeset) do
    %{
      errors: format_errors_msg(changeset)
    }
  end

  defp format_errors_msg(changeset) do
    Ecto.Changeset.traverse_errors(changeset, fn {msg, opts} ->
      Enum.reduce(opts, msg, fn {key, value}, acc ->
        String.replace(acc, "%{#{key}}", to_string(value))
      end)
    end)
  end
end
