defmodule EtherPriceWeb.Router do
  use EtherPriceWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", EtherPriceWeb do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
  end

  # We could version the api, but at this stage it'd be a little over-engineered
  scope "/api", EtherPriceWeb do
    pipe_through :api

    # Based on the way twitter handles api names. Using generic names for now.
    # https://developer.twitter.com/en/docs/tweets/search/api-reference/premium-search.html#DataEndpoint
    get "/price/last24hours", PriceController, :last_24_hours
    get "/price/last3days", PriceController, :last_three_days
    get "/price/last7days", PriceController, :last_seven_days

    # User sign up and authentication
    post "/users/signup", UserController, :create
    post "/users/login", SessionController, :create
  end
end
