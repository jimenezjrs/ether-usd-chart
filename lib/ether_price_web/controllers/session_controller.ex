defmodule EtherPriceWeb.SessionController do
  use EtherPriceWeb, :controller

  import Comeonin.Bcrypt, only: [checkpw: 2, dummy_checkpw: 0]

  alias EtherPrice.Accounts

  def create(conn, user_params) do
    case authenticate(conn, user_params) do
      {:ok, user} ->
        token = Phoenix.Token.sign(conn, "user_token", user.id)
        render conn, "login.json", user: user, token: token
      {:error, _, _} ->
        conn
        |> put_status(:bad_request)
        |> render("errors.json")
    end
  end

  # Move to a plug to separate concerns for authentication.
  defp authenticate(conn, %{"email" => email, "password" => given_password}) do
    user = EtherPrice.Repo.get_by(EtherPrice.Accounts.User, email: email)

    cond do
      user && checkpw(given_password, user.password_hash) ->
        {:ok, user}
      user ->
        {:error, :unauthorized, conn}
      true ->
        # fake an authentication to avoit brute force attacks
        dummy_checkpw()
        {:error, :not_found, conn}
    end
  end
end
