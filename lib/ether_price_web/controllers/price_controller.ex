defmodule EtherPriceWeb.PriceController do
  use EtherPriceWeb, :controller

  def last_24_hours(conn, _params) do
    # for now we'll set the params as ETH and USD. But it'll be really easy to
    # support more types of currencies
    case CryptoCompare.get("ETH", "USD", :last_24_hours) do
      {:ok, data} ->
        render(conn, "last_24_hours.json", data: data)
      _ ->
        # We need to handle errors more gracefully
        render(conn, "error.json")
    end
  end

  def last_three_days(conn, _params) do
    # for now we'll set the params as ETH and USD. But it'll be really easy to
    # support more types of currencies
    case CryptoCompare.get("ETH", "USD", :last_three_days) do
      {:ok, data} ->
        render(conn, "last_three_days.json", data: data)
      _ ->
        # We need to handle errors more gracefully
        render(conn, "error.json")
    end
  end

  def last_seven_days(conn, _params) do
    # for now we'll set the params as ETH and USD. But it'll be really easy to
    # support more types of currencies
    case CryptoCompare.get("ETH", "USD", :last_seven_days) do
      {:ok, data} ->
        render(conn, "last_seven_days.json", data: data)
      _ ->
        # We need to handle errors more gracefully
        render(conn, "error.json")
    end
  end
end
