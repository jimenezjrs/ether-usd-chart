defmodule EtherPriceWeb.PageController do
  use EtherPriceWeb, :controller

  def index(conn, _params) do
    conn
    |> put_layout("chart.html")
    |> render("index.html")
  end
end
