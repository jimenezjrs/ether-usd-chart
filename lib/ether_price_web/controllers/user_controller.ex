defmodule EtherPriceWeb.UserController do
  use EtherPriceWeb, :controller

  alias EtherPrice.Accounts

  def create(conn, user_params) do
    case Accounts.create_user(user_params) do
      {:ok, user} ->
        token = Phoenix.Token.sign(conn, "user_token", user.id)
        render conn, "signup.json", user: user, token: token
      {:error, changeset} ->
        conn
        |> put_status(:bad_request)
        |> render("errors.json", changeset: changeset)
    end
  end
end
